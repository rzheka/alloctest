use rand::{
    rngs::SmallRng,
    thread_rng, Rng, SeedableRng,
};
use std::sync::atomic::{AtomicUsize, Ordering, ATOMIC_USIZE_INIT};
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;
use clap::{App, Arg};
use std::str::FromStr;

fn main() {
    let matches = App::new("alloctest")
        .version("0.1")
        .arg(
            Arg::with_name("threads")
                .short("t")
                .long("threads")
                .takes_value(true)
                .required(false)
                .help("number of concurrent allocation threads"),
        )
        .arg(
            Arg::with_name("minalloc")
                .long("min")
                .takes_value(true)
                .required(false)
                .help("Minimum allocation size, in bytes"),
        )
        .arg(
            Arg::with_name("maxalloc")
                .long("max")
                .takes_value(true)
                .required(false)
                .help("Maximum allocation size, in bytes"),
        )
        .arg(
            Arg::with_name("exchange")
                .short("e")
                .long("exchange")
                .takes_value(true)
                .required(false)
                .help("Exchange allocation rate (from 0.0 to 1.0)"),
        )
        .arg(
            Arg::with_name("time")
                .long("time")
                .takes_value(true)
                .required(false)
                .help("Total run time, in seconds"),
        )
        .arg(
            Arg::with_name("memory")
                .long("mem")
                .takes_value(true)
                .required(false)
                .help("Total consumed memory, in bytes"),
        )
        .get_matches();

    let threads = usize::from_str(matches.value_of("threads").unwrap_or("1")).expect("Invalid threads argument");
    let min_alloc = usize::from_str(matches.value_of("minalloc").unwrap_or("16")).expect("Invalid min alloc argument");
    let max_alloc = usize::from_str(matches.value_of("maxalloc").unwrap_or("16384")).expect("Invalid max alloc argument");
    let exchange = f64::from_str(matches.value_of("exchange").unwrap_or("0.01")).expect("Invalid exchange rate argument");
    let time_sec = usize::from_str(matches.value_of("time").unwrap_or("8")).expect("Invalid time argument");
    let memory = usize::from_str(matches.value_of("memory").unwrap_or("268435456")).expect("Invalid memory argument");

    let per_thread_mem = memory / threads;
    let alloc_pool = per_thread_mem / ((min_alloc + max_alloc)/2);

    let config = Config {
        num_threads: threads,
        min_alloc_size: min_alloc,
        max_alloc_size: max_alloc,
        alloc_exchage_rate: exchange,
        run_time_sec: time_sec,
        thread_alloc_pool: alloc_pool,
    };

    println!("Config: {:?}", config);

    let state = Arc::new(GlobalState::new(config));

    let threads = (0..state.config.num_threads)
        .map(|idx| {
            let state = state.clone();
            thread::spawn(move || {
                allocation_thread(idx, state);
            })
        })
        .collect::<Vec<_>>();

    let mon_thread = thread::spawn(|| monitor_allocations());

    threads.into_iter().for_each(|h| h.join().unwrap());
    stop_monitor();
    mon_thread.join().unwrap();
    println!("Done");
}

#[derive(Debug, Clone)]
struct Config {
    num_threads: usize,
    thread_alloc_pool: usize,
    min_alloc_size: usize,
    max_alloc_size: usize,
    run_time_sec: usize,
    alloc_exchage_rate: f64,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            num_threads: 8,
            thread_alloc_pool: 16 * 1024,
            min_alloc_size: 16,
            max_alloc_size: 16 * 1024,
            run_time_sec: 8,
            alloc_exchage_rate: 0.05,
        }
    }
}

struct GlobalState {
    config: Config,
    foreign_allocs: Vec<Mutex<Vec<Allocation>>>,
}

impl GlobalState {
    pub fn new(config: Config) -> GlobalState {
        let num_threads = config.num_threads;
        let foreign_slots = (config.thread_alloc_pool as f64 * config.alloc_exchage_rate) as usize;
        GlobalState {
            config,
            foreign_allocs: (0..num_threads)
                .map(|_| Mutex::new((0..foreign_slots).map(|_| Allocation::new(0)).collect()))
                .collect(),
        }
    }
}

fn allocation_thread(_idx: usize, state: Arc<GlobalState>) {
    let mut locals = (0..state.config.thread_alloc_pool)
        .map(|_| Allocation::new(0))
        .collect::<Vec<_>>();

    let foreign_slots = state.foreign_allocs[0].lock().unwrap().len();
    let mut rng = SmallRng::from_rng(thread_rng()).unwrap();
    let sw = stopwatch::Stopwatch::start_new();

    loop {
        let tight_alloc_count = 10000;
        for _ in 0..10000 {
            let size = rng.gen_range(state.config.min_alloc_size, state.config.max_alloc_size);
            let is_global = rng.gen_bool(state.config.alloc_exchage_rate);
            if !is_global {
                let slot = rng.gen_range(0, locals.len());
                locals[slot] = Allocation::new(size);
            } else {
                let f_idx = rng.gen_range(0, state.config.num_threads);
                let slot = rng.gen_range(0, foreign_slots);
                let f = &state.foreign_allocs[f_idx];
                let a = Allocation::new(size);
                let _prev_a = {
                    ::std::mem::replace(&mut f.lock().unwrap()[slot], a)
                };
            }
        }

        report_allocations(tight_alloc_count);
        if sw.elapsed_ms() as usize / 1000 >= state.config.run_time_sec {
            break;
        }
    }
}

struct Allocation {
    data: Vec<u8>,
}

impl Allocation {
    pub fn new(size: usize) -> Allocation {
        let mut a = Allocation {
            data: Vec::with_capacity(size),
        };
        a.fill();
        a
    }

    fn fill(&mut self) {
        let cap = self.data.capacity();
        unsafe {
            self.data.set_len(cap);
        }

        let mut i = 0;
        while i < cap {
            self.data[i] = (i * 654321) as u8;
            i += 512;
        }
    }
}

const MONITOR_INTERVAL_SEC: usize = 4;
static TOTAL_ALLOCATIONS: AtomicUsize = ATOMIC_USIZE_INIT;
static STOP_MONITOR: AtomicUsize = ATOMIC_USIZE_INIT;

fn report_allocations(count: usize) {
    TOTAL_ALLOCATIONS.fetch_add(count, Ordering::SeqCst);
}

fn monitor_allocations() {
    loop {
        let prev_allocations = TOTAL_ALLOCATIONS.load(Ordering::SeqCst);
        thread::sleep(Duration::from_secs(MONITOR_INTERVAL_SEC as u64));
        let curr_allocations = TOTAL_ALLOCATIONS.load(Ordering::SeqCst);
        let count = curr_allocations - prev_allocations;
        let rate = count / MONITOR_INTERVAL_SEC;
        println!(".... {} allocations/sec", rate);
        if STOP_MONITOR.load(Ordering::Relaxed) != 0 {
            break;
        }
    }
}

fn stop_monitor() {
    STOP_MONITOR.store(1, Ordering::Relaxed);
}
